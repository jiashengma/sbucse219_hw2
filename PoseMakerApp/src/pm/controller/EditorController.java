
package pm.controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.imageio.ImageIO;
import pm.PoseMaker;
import pm.data.DataManager;
import pm.file.FileManager;
import pm.gui.Workspace;
import saf.controller.AppFileController;
import saf.ui.AppMessageDialogSingleton;

/**
 * Controller class of tool editor.
 * @author Jia Sheng Ma (109696764)
 * @version 1.0
 */
public class EditorController {
    
    PoseMaker app;
    DataManager dataManager;    
    
    String currentTool;
    Rect rect;
    Ellip ellipse;
    double initX;
    double initY;
//    double initX_moveShape;
//    double initY_moveShape;
    boolean isSelection_Tool;    
    Shape prevSelectedShape;
    Shape selectedShape;
    String toolSelected;
    
    double x0;
    double y0;
    double translateX0;
    double translateY0;
    
    private boolean workspaceEnabled;
    
    public EditorController(PoseMaker app) throws Exception {
       this.app = app;
       dataManager = new DataManager(app);
       
       toolSelected = Workspace.toolSelected;
       isSelection_Tool = false;
       currentTool = "";
    }

    public void enable(boolean workspaceStatus) {
        workspaceEnabled = workspaceStatus;
    }

    public void setCurrentTool(String tool) {
        // if previsouly the selection tool is selected
        // and the tool pane exculsed button is selected then true
        currentTool = tool;
        if(tool.equals(Workspace.RECTANGLE_TOOL)) {
            isSelection_Tool = false;
        } else if(tool.equals(Workspace.ELLIPSE_TOOL)) {
            isSelection_Tool = false;
        } else if(tool.equals(Workspace.REMOVAL_TOOL)) {
            isSelection_Tool = false;
        } else if(tool.equals(Workspace.SELECTION_TOOL)) {
            isSelection_Tool = true;
        } else {
            if(getIsSeletionTool()) {
                isSelection_Tool = true;
            } else {
                isSelection_Tool = false;
            }
        }

    }
    
    public void handleRemoveElementRequest(Pane pane) {
        if(workspaceEnabled){
            // Remove selected shape from canvas
            pane.getChildren().remove(selectedShape);
            // Remove selected shape from container
            dataManager.removeShapeFromContainer(selectedShape);
            // MARK THE WORKSPACE AS EDITED
            AppFileController appFileController = (AppFileController)app.getFileController();
            appFileController.markAsEdited(app.getGUI());
        }
    }

    public void handleMoveToFrontRequest() {
        if(workspaceEnabled) {
            if(selectedShape != null) {
                selectedShape.toFront();
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            }
        }
    }

    public void handleMoveToBackRequest() {
        if(workspaceEnabled) {    
            if(selectedShape != null) {
                selectedShape.toBack();
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            }
            
        }
    }

    /**
     * Changes fill color for shapes.     
     * @param color fill color for shapes.
     */
    public void handleChangeFillColor(Color color) { 
        if(workspaceEnabled) {
            ((Workspace)app.getWorkspaceComponent()).setFillColorPicker(color);
            if(selectedShape != null) {
                selectedShape.setFill(color);
            }
            // MARK THE WORKSPACE AS EDITED
            AppFileController appFileController = (AppFileController)app.getFileController();
            appFileController.markAsEdited(app.getGUI());
        }
    }

    /**
     * Changes outline color of selected shape.     
     * @param color 
     */
    public void handleChangeOutlineColor(Color color) {
        if(workspaceEnabled) {
            ((Workspace)app.getWorkspaceComponent()).setOutlineColorPicker(color);
            if(selectedShape != null) {
                selectedShape.setStroke(color);
                if(selectedShape instanceof Rect) {
                    ((Rect)selectedShape).setStrokeColor(color);
                }
                else {
                    ((Ellip)selectedShape).setStrokeColor(color);
                }
            } 
            // MARK THE WORKSPACE AS EDITED
            AppFileController appFileController = (AppFileController)app.getFileController();
            appFileController.markAsEdited(app.getGUI());
        }
    }

    /**
     * Handles background color change request.
     * Changes background color to that of color picker's value.
     * @param pane whose background color to be changed.
     * @param color background color.
     */
    public void handleChangeBackgroundColor(Pane pane, Color color) {
        if(workspaceEnabled) {
            pane.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
            // MARK THE WORKSPACE AS EDITED
            AppFileController appFileController = (AppFileController)app.getFileController();
            appFileController.markAsEdited(app.getGUI());
        }
    }

    /**
     * Changes outline thickness of selected shape.
     * 
     * @param value     
     */
    public void handleChangeOutlineThickness(double value) {
        if(workspaceEnabled) {
            ((Workspace)app.getWorkspaceComponent()).setOutlineThickness_slider(value);
            if(selectedShape != null) {
                selectedShape.setStrokeWidth(value);
            }
            // MARK THE WORKSPACE AS EDITED
            AppFileController appFileController = (AppFileController)app.getFileController();
            appFileController.markAsEdited(app.getGUI());
        }
    }
    
    /**
     * 
     * @param pane     
     * @param startingLocation
     * @param endLocation
     * @param currentTool 
     */
    public void handleMousePressed(Pane pane, Point2D startingLocation, Point2D endLocation, String currentTool) {
        if(workspaceEnabled) {
            initX = startingLocation.getX();
            initY = startingLocation.getY();
            if(currentTool.equals(Workspace.RECTANGLE_TOOL)) {
                isSelection_Tool = false;
                rect = rectPreset();
                rect.setX(initX);
                rect.setY(initY);

                // select this shape
                prevSelectedShape = selectedShape;
                if(prevSelectedShape != null) {
                    if(prevSelectedShape instanceof Rect) {
                        prevSelectedShape.setStroke(((Rect)prevSelectedShape).getStrokeColor());                        
                    }
                    else {
                        prevSelectedShape.setStroke(((Ellip)prevSelectedShape).getStrokeColor());                        
                    }
                }            

                // Add shape to canvas
                pane.getChildren().add(rect);
                selectedShape = rect;               // select the shape
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            } else if(currentTool.equals(Workspace.ELLIPSE_TOOL)) {
                isSelection_Tool = false;
                ellipse = ellipsePreset();
                ellipse.setCenterX(initX);
                ellipse.setCenterY(initY);
                // select this shape
                prevSelectedShape = selectedShape;
                if(selectedShape != null) {
                    if(selectedShape instanceof Rect) {
                        prevSelectedShape.setStroke(((Rect)selectedShape).getStrokeColor());
                    }
                    else {
                        prevSelectedShape.setStroke(((Ellip)selectedShape).getStrokeColor());
                    }
                }
                // Add shape to canvas
                pane.getChildren().add(ellipse);             
                selectedShape = ellipse;            // select the shape
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            }                
        }
    }
    
    public void handleMouseDragged(Point2D startingLocation, Point2D endLocation, String toolSelected) {
        if(workspaceEnabled) {
            if(toolSelected.equals(Workspace.RECTANGLE_TOOL)) {
                isSelection_Tool = false;
                double offsetX = endLocation.getX() - initX;
                double offsetY = endLocation.getY() - initY;

                if( offsetX > 0)
                    rect.setWidth(offsetX);
                else {
                    rect.setX(endLocation.getX());
                    rect.setWidth(initX - endLocation.getX());
                }

                if( offsetY > 0) {
                    rect.setHeight(offsetY);
                } else {
                    rect.setY(endLocation.getY());
                    rect.setHeight(initY - endLocation.getY());
                }
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            } else if(toolSelected.equals(Workspace.ELLIPSE_TOOL)) {
                isSelection_Tool = false;
                double offsetX = endLocation.getX() - initX;
                double offsetY = endLocation.getY() - initY;
                ellipse.setRadiusX(Math.abs(offsetX));
                ellipse.setRadiusY(Math.abs(offsetY));
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            }
            
        }
        
    }

    public void handleMouseReleased(Pane pane, Point2D startingLocation, Point2D endLocation, String toolSelected) {
        if(workspaceEnabled) {
            if(toolSelected.equals(Workspace.RECTANGLE_TOOL)) {
                
                rect.setStroke(Color.GOLD);
                
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            } else if(toolSelected.equals(Workspace.ELLIPSE_TOOL)) {
                ellipse.setStroke(Color.GOLD);
                
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            }
        }
    }
    double offsetX;
    double offsetY;
    /**
     * Provides the presets of a rectangle.     
     * @return rectangle with presets
     */
    public Rect rectPreset() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        Paint fill = workspace.getFillColorPicker().getValue();
        Paint outline = workspace.getOutlineColorPicker().getValue();
        double thickness = workspace.getOutlineThickness_slider().getValue();
        
        Rect rect = new Rect();
        rect.setFill(fill);                 // set outline color        
        rect.setStrokeWidth(thickness);     // Default stroke width
        rect.setStroke(outline);            // set stroke        
        rect.setStrokeColor((Color)outline);// store stroke     
        
        rect.setOnMousePressed(e->{

            x0 = e.getSceneX();
            y0 = e.getSceneY();
            translateX0 = rect.getTranslateX();
            translateY0 = rect.getTranslateY();
            
            handleSelectShape(rect/*, isSelection_Tool*/);
        });

        rect.setOnMouseDragged(e ->{
            handleDragShape(rect/*, isSelection_Tool*/);
        });

        return rect;
    }
    
    /**
     * Provides the presets of a ellipse.     
     * @return a ellipse with presets.
     */
    public Ellip ellipsePreset() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        Paint fill = workspace.getFillColorPicker().getValue();
        Paint outline = workspace.getOutlineColorPicker().getValue();
        double thickness = workspace.getOutlineThickness_slider().getValue();
        
        Ellip ellip = new Ellip();                
        ellip.setFill(fill);                    // set outline color         
        ellip.setStrokeWidth(thickness);                        // Default stroke width
        ellip.setStroke(outline);                // set stroke        
        ellip.setStrokeColor((Color)outline);    // store stroke        
        // Add handlers
        ellip.setOnMousePressed(e->{

            x0 = e.getSceneX();
            y0 = e.getSceneY();
            translateX0 = ellip.getTranslateX();
            translateY0 = ellip.getTranslateY();
            
            handleSelectShape(ellip/*, isSelection_Tool*/);
        });
        
        ellip.setOnMouseDragged(e->{
            handleDragShape(ellip/*, isSelection_Tool*/);            
        });
        return ellip;    
    }
    
    /**
     * Drags shape to where the mouse leads it to.
     * @param shape selected shape.
     */
    public void handleDragShape(Shape shape/*, boolean isSelectionOn*/) {
        if(workspaceEnabled) {
            if(isSelection_Tool) {
                shape.setOnMouseDragged(e ->{
                    if(shape instanceof Rect) {                        
                        Rect r = rect;
                        offsetX = e.getSceneX() - x0;
                        offsetY = e.getSceneY() - y0;
                        r.setTranslateX(translateX0 + offsetX);
                        r.setTranslateY(translateY0 + offsetY);
                        
                    } else if(shape instanceof Ellip) {
                        Ellip el = ellipse;
                        offsetX = e.getSceneX() - x0;
                        offsetY = e.getSceneY() - y0;
                        el.setTranslateX(translateX0 + offsetX);
                        el.setTranslateY(translateY0 + offsetY);
                    }
                }); 
                // MARK THE WORKSPACE AS EDITED
                AppFileController appFileController = (AppFileController)app.getFileController();
                appFileController.markAsEdited(app.getGUI());
            }
        }
    }
    
    /**
     * Assigns the selected shape to the variable selectedShape for use.
     * @param shape selected shape.
     */
    public void handleSelectShape(Shape shape/*, boolean isSelectionOn*/) {
        if(workspaceEnabled) {
            if(isSelection_Tool) {
                prevSelectedShape = selectedShape;      // keep the reference of the previous selected shape
                if(prevSelectedShape != null) {
                    if(prevSelectedShape instanceof Rect)
                        prevSelectedShape.setStroke(((Rect)prevSelectedShape).getStrokeColor());
                    else
                        prevSelectedShape.setStroke(((Ellip)prevSelectedShape).getStrokeColor());                
                }

                selectedShape = shape;                  // set the new selected shape
                selectedShape.setStroke(Color.GOLD);    // highlight shape

                Workspace workspace = (Workspace)app.getWorkspaceComponent();
                // update tool pane settings to reflect selected shape
                workspace.setFillColorPicker((Color)selectedShape.getFill());
                workspace.setOutlineThickness_slider(selectedShape.getStrokeWidth());
                if(selectedShape instanceof Rect)
                    workspace.setOutlineColorPicker((Color)((Rect)selectedShape).getStrokeColor());
                else 
                    workspace.setOutlineColorPicker((Color)((Ellip)selectedShape).getStrokeColor());
            }
            
        }
    }
    
    public void takeSnapShot(Pane drawing_Pane) {
        WritableImage snapshot = drawing_Pane.snapshot(null, null);        
        ImageView imageView = new ImageView(snapshot);

            // PROMPT THE USER FOR A FILE NAME
            FileChooser fc = new FileChooser();
            File snapshot_dir = new File(FileManager.SNAPSHOT_PATH);
            if(!snapshot_dir.exists()){
                snapshot_dir.mkdir();
            }
            fc.setInitialDirectory(snapshot_dir);
            fc.setTitle("Save snapshot");
            fc.getExtensionFilters().addAll(new ExtensionFilter("Snapshot", "*.png"));

            File file = fc.showSaveDialog(app.getGUI().getWindow());
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(imageView.getImage(),
                        new BufferedImage((int)imageView.getImage().getWidth(), 
                                (int)imageView.getImage().getHeight(), 1)), "png", file);
        } catch (IOException ioe) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show("Save Snapshot", "Error in saving snapshot.");
        }
    }
    
    public void setPrevSelectedShape(Shape s) { prevSelectedShape = s; }
    public Shape getPrevSelectedShape() { return prevSelectedShape;}
    
    public void setSelectedShape(Shape s) { selectedShape = s; }
    public Shape getSelectedShape() { return selectedShape;}
    
    public void setIsSelectionTool(String t) {
        isSelection_Tool = (t.equals(Workspace.SELECTION_TOOL));
    }
    public boolean getIsSeletionTool() {
        return isSelection_Tool;
    }
    
    public void enableAllBtns(Button btn, ArrayList<Button> disableable_Btns) {        
        for(Button b : disableable_Btns) {
           b.setDisable(false); 
        }
    }
}