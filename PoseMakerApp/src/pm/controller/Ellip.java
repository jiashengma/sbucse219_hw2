
package pm.controller;

import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import pm.gui.Workspace;

/**
 *
 * @author Jia Sheng Ma (109696764)
 * @version 1.1
 */
public class Ellip extends Ellipse {
    Color strokeColor;    
    public void setStrokeColor(Color c) {strokeColor = c;}
    public Color getStrokeColor() {return strokeColor; }
    
    public void handleSelect(String tool, EditorController ec, Workspace workspace ) {
        Shape prevSelectedShape = ec.getPrevSelectedShape();        
        if(tool.equals(Workspace.SELECTION_TOOL)) {            
                ec.setPrevSelectedShape(ec.getSelectedShape());      // keep the reference of the previous selected shape
                if(ec.getPrevSelectedShape() != null) {
                    if(ec.getPrevSelectedShape() instanceof Rect)
                        ec.getPrevSelectedShape().setStroke(((Rect)ec.getPrevSelectedShape()).getStrokeColor());
                    else
                        ec.getPrevSelectedShape().setStroke(((Ellip)ec.getPrevSelectedShape()).getStrokeColor());                
                }

                ec.setSelectedShape(this);                  // set the new selected shape
                ec.getSelectedShape().setStroke(Color.GOLD);    // highlight shape
                
                // update tool pane settings to reflect selected shape
                workspace.setFillColorPicker((Color)ec.getSelectedShape().getFill());
                workspace.setOutlineThickness_slider(ec.getSelectedShape().getStrokeWidth());
                if(ec.getSelectedShape() instanceof Rect)
                    workspace.setOutlineColorPicker((Color)((Rect)ec.getSelectedShape()).getStrokeColor());
                else 
                    workspace.setOutlineColorPicker((Color)((Ellip)ec.getSelectedShape()).getStrokeColor());

            }
    }
    public void handleDrag(String tool, double x, double y) {
        if(tool.equals(Workspace.SELECTION_TOOL)) {
            this.setCenterX(x);
            this.setCenterY(y);
        }
    }
    
    
}
