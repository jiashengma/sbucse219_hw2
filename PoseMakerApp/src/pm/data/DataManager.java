package pm.data;

import java.util.ArrayList;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Jia Sheng Ma (109696764)
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    ArrayList<Shape> shapes;
    Color bgColor;
    
    Pane drawing_pane;
    /**
     * This constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        shapes = new ArrayList<>();
        bgColor = Color.WHITE;  // default bg color is white
        drawing_pane = new Pane();        
    }

    /**
     * Adds every shape created on the canvas to the shape container.
     * @param shape 
     */
    public void addShape(Shape shape) {
        shapes.add(shape); 
        //DEBUG
        System.out.println("Shape in container: " + shapes.size() + "\n");
    }
    
    /**     
     * @return a list of shape drawn on the canvas.
     */
    public ArrayList<Shape> getShapes() {return shapes; }
    
    /**
     * Removes shape from the shape container.
     * @param shape 
     */
    public void removeShapeFromContainer(Shape shape) {shapes.remove(shape); }
    
    public void setBgColor(Color color) { bgColor = color; }
    public Color getBgColor() {
        //Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //return workspace.getBackgroundColor(); 
        return bgColor;
    }
    
    public Pane getDrawingPane() {
        drawing_pane = ((Workspace)app.getWorkspaceComponent()).getDrawingPane();
        return drawing_pane;
    }
    
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();        
        workspace.getDrawingPane().getChildren().clear();   // clear drawing pane        
        shapes.clear();                                     // clear shapes
        workspace.setBackgroundColor(Color.WHITE);
        setBgColor(Color.WHITE);
        workspace.setFillColorPicker(Color.BLACK);
        workspace.setOutlineColorPicker(Color.BLACK);
        workspace.setOutlineThickness_slider(2);        
    }
}
