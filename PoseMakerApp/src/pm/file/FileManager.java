package pm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.controller.Ellip;
import pm.controller.Rect;
import pm.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author Jia Sheng Ma (109696764)
 * @version 1.1
 */
public class FileManager implements AppFileComponent {
    
    // SAVING THE FOLLOWING TO JSON 
    // make json reader to read background frist and then shapes
    static final String BACKGROUND_COLOR = "background color";
    
    static final String SHAPES = "shapes";
    static final String SHAPE = "shape";
    static final String RECTANGLE = "rectangle";
    static final String ELLIPSE = "ellipse";
    static final String X = "x";
    static final String Y = "y";
    static final String CENTERX = "centerX";
    static final String CENTERY = "centerY";
    static final String WIDTH = "width";
    static final String HEIGHT = "height";
    static final String RADIUSX = "radiusX";
    static final String RADIUSY = "radiusY";
    static final String FILL_COLOR = "fill";
    static final String OUTLINE_COLOR = "outline color";
    static final String OUTLINE_THICKNESS = "outline thickness";
    
    public static final String SNAPSHOT_PATH = "./snapshot";
    
    private JsonObject makeTagJsonObject(Node shape) {        
        if(shape instanceof Rect){
            Rect r = (Rect)shape;
            JsonObject jso = Json.createObjectBuilder()
		.add(SHAPE, RECTANGLE)
		.add(X, r.getX()+"")
		.add(Y, r.getY()+"")
		.add(WIDTH, r.getWidth()+"")
		.add(HEIGHT, r.getHeight()+"")
		.add(FILL_COLOR, r.getFill().toString())
		.add(OUTLINE_COLOR, r.getStrokeColor().toString())
                .add(OUTLINE_THICKNESS, r.getStrokeWidth()+"")
		.build();
            return jso;
        } else {
            Ellip e = (Ellip)shape;
            JsonObject jso = Json.createObjectBuilder()
		.add(SHAPE, ELLIPSE)
		.add(CENTERX, e.getCenterX()+"")
		.add(CENTERY, e.getCenterY()+"")
		.add(RADIUSX, e.getRadiusX()+"")
		.add(RADIUSY, e.getRadiusY()+"")
		.add(FILL_COLOR, e.getFill().toString())
		.add(OUTLINE_COLOR, e.getStrokeColor().toString())
                .add(OUTLINE_THICKNESS,e.getStrokeWidth()+"")
		.build();
            return jso;
        }
    }
    private void fillArrayWithShapes(ObservableList<Node> shapes, JsonArrayBuilder arrayBuilder) {	
        for(Node s : shapes) {            
            JsonObject tagObject = makeTagJsonObject(s);
            arrayBuilder.add(tagObject);
        }	
    }
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	StringWriter sw = new StringWriter();
	
	DataManager dataManager = (DataManager)data;
        
        //ArrayList<Shape> shapes = dataManager.getShapes();
        ObservableList<Node> shapes = dataManager.getDrawingPane().getChildren();
        
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        fillArrayWithShapes(shapes, arrayBuilder);
	JsonArray shapeArray = arrayBuilder.build();
	
	// PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()		
		.add(SHAPES, shapeArray)
                .add(BACKGROUND_COLOR, dataManager.getBgColor().toString())
		.build();
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject jso = loadJSONFile(filePath);
        JsonArray jsonShapesArray = jso.getJsonArray(SHAPES);
        
        // LOAD THE SHAPES TO WORKSPACE
        //DEBUGG
        System.out.println(">>Before load shape\n");
        loadShapes(jsonShapesArray, dataManager);
        //DEBUGG
        System.out.println("After load shape <<");
        
        String bgColor = jso.getString(BACKGROUND_COLOR).substring(0, jso.getString(BACKGROUND_COLOR).length()-2);        
        // parse color, the dangling "ff" should be removed
        dataManager.setBgColor((Color)Paint.valueOf(bgColor));
        System.out.println("Background color: " + bgColor);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private void loadShapes(JsonArray jsonShapesArray, DataManager dataManager) {
        for(int i = 0; i < jsonShapesArray.size(); i++) {
            JsonObject jso = jsonShapesArray.getJsonObject(i);
            Shape shape = loadShape(jso);
            if(shape instanceof Rect) {                
                //Rect r = (Rect)loadShape(jsonShapesArray.getJsonObject(i));
                Rect r = (Rect)shape;
                dataManager.addShape(r);
            } else if(shape instanceof Ellip){                
                //Ellip e = (Ellip)loadShape(jsonShapesArray.getJsonObject(i));
                Ellip e = (Ellip)shape;
                dataManager.addShape(e);
            }
        }        
    }
    
    private Shape loadShape(JsonObject jso) {
        boolean isRect = jso.getString(SHAPE).equals(RECTANGLE);        
        if(isRect) {
            System.out.println("Rect");
            Rect r = new Rect();
            r.setX(Double.parseDouble(jso.getString(X)));
            System.out.println("x loaded");            
            r.setY(Double.parseDouble(jso.getString(Y)));
            System.out.println("y loaded");
            r.setWidth(Double.parseDouble(jso.getString(WIDTH)));
            System.out.println("w loaded");
            r.setHeight(Double.parseDouble(jso.getString(HEIGHT)));
            System.out.println("h loaded");
            r.setFill(Paint.valueOf(jso.getString(FILL_COLOR).substring(0, jso.getString(FILL_COLOR).length()-2)));
            System.out.println("fill loaded");
            r.setStrokeColor((Color)Paint.valueOf(jso.getString(OUTLINE_COLOR).substring(0, jso.getString(OUTLINE_COLOR).length()-2)));
            System.out.println("intrinsic stroke loaded");
            r.setStroke(Paint.valueOf(jso.getString(OUTLINE_COLOR).substring(0, jso.getString(OUTLINE_COLOR).length()-2)));
            System.out.println("stroke loaded");
            r.setStrokeWidth(Double.parseDouble(jso.getString(OUTLINE_THICKNESS)));
            System.out.println("thickness loaded\n");
            return r;
        } else {
            System.out.println("Ellip");
            Ellip e = new Ellip();            
            e.setCenterX(Double.parseDouble(jso.getString(CENTERX)));
            System.out.println("center x loaded");
            e.setCenterY(Double.parseDouble(jso.getString(CENTERY)));
            System.out.println("center y loaded");
            e.setRadiusX(Double.parseDouble(jso.getString(RADIUSX)));
            System.out.println("radius x loaded");
            e.setRadiusY(Double.parseDouble(jso.getString(RADIUSY)));
            System.out.println("radius y loaded");
            e.setFill(Paint.valueOf(jso.getString(FILL_COLOR).substring(0, jso.getString(FILL_COLOR).length()-2)));
            System.out.println("fill loaded");
            e.setStrokeColor((Color)Paint.valueOf(jso.getString(OUTLINE_COLOR).substring(0, jso.getString(OUTLINE_COLOR).length()-2)));
            System.out.println("intrinsic stroke loaded");
            e.setStroke(Paint.valueOf(jso.getString(OUTLINE_COLOR).substring(0, jso.getString(OUTLINE_COLOR).length()-2)));
            System.out.println("stroke loaded");
            e.setStrokeWidth(Double.parseDouble(jso.getString(OUTLINE_THICKNESS)));  
            System.out.println("thickness loaded\n");
            return e;
        }
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
