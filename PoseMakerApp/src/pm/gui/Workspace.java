package pm.gui;


import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import javafx.stage.Screen;
import pm.PoseMaker;
import pm.PropertyType;
import pm.controller.EditorController;
import pm.controller.Ellip;
import pm.controller.Rect;
import pm.data.DataManager;
import pm.file.FileManager;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Jia Sheng Ma (109696764)
 * @version 1.1
 */
public class Workspace extends AppWorkspaceComponent {
    
    public static final String RECTANGLE_TOOL = "Rectangle tool";
    public static final String ELLIPSE_TOOL = "Ellipse tool";
    public static final String SELECTION_TOOL = "Selection tool";
    public static final String REMOVAL_TOOL = "Removal tool";
    static final String MOVETOBACK = "Move to back";
    static final String MOVETOFRONT = "Move to front";
    static final String BACKGROUNDCOLOR = "background color picker";
    static final String FILLCOLOR = "fill color picker";
    static final String OUTLINECOLOR = "outline color picker";
    static final String OUTLINETHICKNESS = "outline thickness";
    static final String SNAPSHOT = "snap shot";
    static final int BUTTON_TAG_WIDTH = 30;
    static final String SHAPE_EDIT_BTN_STYLE_CLASS = "shape_Btn";
    static final String CANVAS_STYLE_CLASS = "canvas";
    static final String TOOLS_PANE_STYLE_CLASS = "toolsPane";
    static final String HBOX_STYLE_CLASS = "hboxes";
    static final String VBOX_STYLE_CLASS = "vboxes";
    static final String LABEL_STYLE_CLASS = "label";
    
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    EditorController editorController;   
    
    ArrayList<Button> shapeEditor_Btns;
    ArrayList<Button> disableable_Btns;
    Button selectionTool_Btn;
    Button remove_Btn;
    Button rectangle_Btn;
    Button ellipse_Btn;
    Button moveToFront_Btn;
    Button moveToBack_Btn;
    
    Paint paintBoard;
    
    VBox tools_Pane;
    HBox shapeEditor_Pane;
    HBox moveBtn_Pane;
    
    VBox backgroundColor_Pane;    
    ColorPicker backgroundColorPicker;
    Label backgroundColor_lb;
    
    VBox fillColor_Pane;
    ColorPicker fillColorPicker;
    Label fillColor_lb;
    
    VBox outlineColor_Pane;
    ColorPicker outlineColorPicker;
    Label outlineColor_lb;
    
    VBox outlineThickness_Pane;
    Slider outlineThickness_slider;
    Label outlineThickness_lb;
    double thickness;
    
    VBox snapshot_Pane;
    Button snapshot_Btn;
    
    HBox workspaceContainer;
    Pane drawing_Pane;
    
    public static String toolSelected;
    static final String IDLE = "idle";
    
    Point2D startingLocation;
    Point2D endLocation;
        
    Scene primaryScene;

    DataManager dataManager;
    FileManager fileManager;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException, Exception {
	// KEEP THIS FOR LATER
	app = initApp;
	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        primaryScene = gui.getPrimaryScene();
        
        editorController = new EditorController((PoseMaker)this.app); 
        fileManager = (FileManager)app.getFileComponent();
        dataManager= (DataManager)app.getDataComponent();
        PropertiesManager propsSingleton = PropertiesManager.getPropertiesManager();
        toolSelected = IDLE;
        disableable_Btns = new ArrayList<>();
        /**********************INITIALIZE CONTAINER FOR EDITORS***********************/
        // Split the workspace with a split pane
        workspaceContainer = new HBox();
        // child one is tools_Pane for tools         
        tools_Pane = new VBox();
                
        /****************************Shapes Editor********************************/
        // INITIALIZE CONTAINER FOR SHAPES EDITOR
        shapeEditor_Pane = new HBox();        
        shapeEditor_Btns = new ArrayList<>();
        // Initialize Selection tool Button
        selectionTool_Btn = gui.initChildButton(shapeEditor_Pane, 
                        PropertyType.SELECTION_TOOL_ICON.toString(), 
                        PropertyType.SELECTION_TOOL_TOOLTIP.toString(), 
                        false);
        disableable_Btns.add(selectionTool_Btn);
	selectionTool_Btn.setMaxWidth(BUTTON_TAG_WIDTH);
	selectionTool_Btn.setMinWidth(BUTTON_TAG_WIDTH);
	selectionTool_Btn.setPrefWidth(BUTTON_TAG_WIDTH);
	selectionTool_Btn.setOnAction(e -> {	    
            toolSelected = SELECTION_TOOL;
            drawing_Pane.setCursor(Cursor.DEFAULT);
            editorController.setCurrentTool(toolSelected);
            //editorController.setIsSelectionTool(toolSelected);
            //editorController.setDisableBtn(selectionTool_Btn, disableable_Btns);            
            for(Button b : disableable_Btns) {
                if(b.equals(selectionTool_Btn)) {
                    b.setDisable(true);
                } else {
                    b.setDisable(false);
                }          
            }
            
	});
        // Initialize Remove Button
        remove_Btn = gui.initChildButton(shapeEditor_Pane, 
                        PropertyType.REMOVE_ICON.toString(), 
                        PropertyType.REMOVE_TOOLTIP.toString(), 
                        false);
        disableable_Btns.add(remove_Btn);
	remove_Btn.setMaxWidth(BUTTON_TAG_WIDTH);
	remove_Btn.setMinWidth(BUTTON_TAG_WIDTH);
	remove_Btn.setPrefWidth(BUTTON_TAG_WIDTH);
	remove_Btn.setOnAction(e -> {
            drawing_Pane.setCursor(Cursor.DEFAULT);
	    editorController.handleRemoveElementRequest(drawing_Pane);
            toolSelected = REMOVAL_TOOL;
            editorController.setCurrentTool(toolSelected); 
            //editorController.setIsSelectionTool(toolSelected);
            editorController.enableAllBtns(remove_Btn, disableable_Btns);
            moveToBack_Btn.setDisable(true);
            moveToFront_Btn.setDisable(true);
	});
        // Initialize Rectangle Button
        rectangle_Btn = gui.initChildButton(shapeEditor_Pane, 
                        PropertyType.RECTANGLE_ICON.toString(), 
                        PropertyType.RECTANGLE_TOOLTIP.toString(), 
                        false);
        disableable_Btns.add(rectangle_Btn);
	rectangle_Btn.setMaxWidth(BUTTON_TAG_WIDTH);
	rectangle_Btn.setMinWidth(BUTTON_TAG_WIDTH);
	rectangle_Btn.setPrefWidth(BUTTON_TAG_WIDTH);
	rectangle_Btn.setOnAction(e -> {	    
            drawing_Pane.setCursor(Cursor.CROSSHAIR);
            toolSelected = RECTANGLE_TOOL;
            editorController.setCurrentTool(toolSelected);
            //editorController.setIsSelectionTool(toolSelected);
            for(Button b : disableable_Btns) {
                if(b.equals(rectangle_Btn)) {
                    b.setDisable(true);
                } else {
                    b.setDisable(false);
                }
            }
            moveToBack_Btn.setDisable(true);
            moveToFront_Btn.setDisable(true);
	});
        
        // Initialize Ellipse Button
        ellipse_Btn = gui.initChildButton(shapeEditor_Pane, 
                        PropertyType.ELLIPSE_ICON.toString(), 
                        PropertyType.ELLIPSE_TOOLTIP.toString(), 
                        false);
        disableable_Btns.add(ellipse_Btn);
	ellipse_Btn.setMaxWidth(BUTTON_TAG_WIDTH);
	ellipse_Btn.setMinWidth(BUTTON_TAG_WIDTH);
	ellipse_Btn.setPrefWidth(BUTTON_TAG_WIDTH);
	ellipse_Btn.setOnAction(e -> {	    
            drawing_Pane.setCursor(Cursor.CROSSHAIR);
            toolSelected = ELLIPSE_TOOL;
            editorController.setCurrentTool(toolSelected);
            //editorController.setIsSelectionTool(toolSelected);
            for(Button b : disableable_Btns) {
                if(b.equals(ellipse_Btn)) {
                    b.setDisable(true);
                } else {
                    b.setDisable(false);
                }
            }
            moveToBack_Btn.setDisable(true);
            moveToFront_Btn.setDisable(true);
	});
        
        shapeEditor_Btns.add(selectionTool_Btn);
        shapeEditor_Btns.add(remove_Btn);
        shapeEditor_Btns.add(rectangle_Btn);
        shapeEditor_Btns.add(ellipse_Btn);                        
                
        // Add shape editor to editor Pane
        tools_Pane.getChildren().add(shapeEditor_Pane);
       
        /****************************Move to Front/Back**************************/
        // Initialize container for move buttons
        moveBtn_Pane = new HBox();
        moveToFront_Btn = gui.initChildButton(moveBtn_Pane, 
                            PropertyType.MOVETOFRONT_ICON.toString(), 
                            PropertyType.MOVETOFRONT_TOOLTIP.toString(), 
                            false);
        disableable_Btns.add(moveToFront_Btn);
	moveToFront_Btn.setMaxWidth(BUTTON_TAG_WIDTH);
	moveToFront_Btn.setMinWidth(BUTTON_TAG_WIDTH);
	moveToFront_Btn.setPrefWidth(BUTTON_TAG_WIDTH);
	moveToFront_Btn.setOnAction(e -> {
	    editorController.handleMoveToFrontRequest();
//            toolSelected = MOVETOFRONT;
//            editorController.setCurrentTool(toolSelected);
	});
        moveToFront_Btn.setMinWidth(100);
    
        moveToBack_Btn = gui.initChildButton(moveBtn_Pane, 
                            PropertyType.MOVETOBACK_ICON.toString(), 
                            PropertyType.MOVETOBACK_TOOLTIP.toString(), 
                            false);
        disableable_Btns.add(moveToBack_Btn);
	moveToBack_Btn.setMaxWidth(BUTTON_TAG_WIDTH);
	moveToBack_Btn.setMinWidth(BUTTON_TAG_WIDTH);
	moveToBack_Btn.setPrefWidth(BUTTON_TAG_WIDTH);
	moveToBack_Btn.setOnAction(e -> {
	    editorController.handleMoveToBackRequest();            
//            toolSelected = MOVETOBACK;
//            editorController.setCurrentTool(toolSelected);                       
	});
        moveToBack_Btn.setMinWidth(100);
        moveBtn_Pane.setAlignment(Pos.CENTER);
        // Add moveBtn_Pane to editor Pane
        tools_Pane.getChildren().add(moveBtn_Pane); 
        
        /**********************************Background Color********************************/
        backgroundColor_Pane = new VBox();
        backgroundColorPicker = new ColorPicker(); // Initialize background color to be white
        backgroundColorPicker.setValue(Color.WHITE);
        
        backgroundColorPicker.setOnMouseClicked(e ->{
//            toolSelected = BACKGROUNDCOLOR;
//            editorController.setCurrentTool(toolSelected);
        });
        backgroundColorPicker.setOnAction(e -> {
            // Set canvas background to backgroundColorPicker.getValue();
            editorController.handleChangeBackgroundColor(drawing_Pane, backgroundColorPicker.getValue());
            dataManager.setBgColor(backgroundColorPicker.getValue());
//            toolSelected = BACKGROUNDCOLOR;
//            editorController.setCurrentTool(toolSelected);
        });        
        backgroundColor_lb = new Label("Background Color");        
        backgroundColor_Pane.getChildren().addAll(backgroundColor_lb, backgroundColorPicker);
        tools_Pane.getChildren().add(backgroundColor_Pane);
        
        /*************************************Fill Color********************************/
        fillColor_Pane = new VBox();
        fillColorPicker = new ColorPicker();
        // Initialize fill color to be black
        fillColorPicker.setValue(Color.BLACK);  
        
        fillColorPicker.setOnMouseClicked(e -> {            
//            toolSelected = FILLCOLOR;
//            editorController.setCurrentTool(toolSelected);
        });
        fillColorPicker.setOnAction(e -> {                        
            editorController.handleChangeFillColor(fillColorPicker.getValue());
//            toolSelected = FILLCOLOR;
//            editorController.setCurrentTool(toolSelected);
        });
        fillColor_lb = new Label("Fill Color");        
        fillColor_Pane.getChildren().addAll(fillColor_lb, fillColorPicker);
        tools_Pane.getChildren().add(fillColor_Pane);
        
        /**********************************Outline Color********************************/
        outlineColor_Pane = new VBox();
        outlineColorPicker = new ColorPicker();
        // Initilize outline color to be black
        outlineColorPicker.setValue(Color.BLACK);
        outlineColorPicker.setOnMouseClicked(e -> {            
//            toolSelected = OUTLINECOLOR;
//            editorController.setCurrentTool(toolSelected);
        });
        outlineColorPicker.setOnAction(e -> {
            // Set outline color to outlineColorPicker.getValue();
            editorController.handleChangeOutlineColor(outlineColorPicker.getValue());
//            toolSelected = OUTLINECOLOR;
//            editorController.setCurrentTool(toolSelected);
        });        
        outlineColor_lb = new Label("Outline Color");
        outlineColor_Pane.getChildren().addAll(outlineColor_lb, outlineColorPicker);
        tools_Pane.getChildren().add(outlineColor_Pane);
        
        /****************************Outline Thickness*********************************/
        outlineThickness_Pane = new VBox();
        outlineThickness_slider = new Slider(2, 20, 2); 
        outlineThickness_slider.setOnMousePressed(e -> {            
//            toolSelected = OUTLINETHICKNESS;
//            editorController.setCurrentTool(toolSelected);
        });
        outlineThickness_slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                editorController.handleChangeOutlineThickness(newValue.doubleValue());
//                toolSelected = OUTLINETHICKNESS;
//                editorController.setCurrentTool(toolSelected);
            }
        });

        outlineThickness_lb = new Label("Outline Thickness");
        outlineThickness_Pane.getChildren().addAll(outlineThickness_lb, outlineThickness_slider);
        tools_Pane.getChildren().add(outlineThickness_Pane);
        
        /**********************************Snap shot**********************************/
        snapshot_Pane = new VBox();
        snapshot_Btn = gui.initChildButton(snapshot_Pane, 
                        PropertyType.SNAPSHOT_ICON.toString(), 
                        PropertyType.SELECTION_TOOL_TOOLTIP.toString(), 
                        false);
        snapshot_Btn.setOnAction( e -> {            
            editorController.takeSnapShot(drawing_Pane);
//            toolSelected = SNAPSHOT;
//            editorController.setCurrentTool(toolSelected);
        });
        tools_Pane.getChildren().add(snapshot_Pane);
        
        /*********************************Canvas***************************************/        
        
        Screen screen = Screen.getPrimary();                
        drawing_Pane = new Pane();
        drawing_Pane.setPrefSize(screen.getBounds().getWidth() - tools_Pane.getWidth() - 10, screen.getBounds().getHeight());
                
        // child two is canvas for drawing
        drawing_Pane.setStyle("-fx-background-color: white");
        workspaceContainer.getChildren().add(tools_Pane);
        workspaceContainer.getChildren().add(drawing_Pane);
        
        drawing_Pane.setOnMousePressed(e -> {            
            startingLocation = new Point2D(e.getX(), e.getY());              
            endLocation = new Point2D(e.getX(), e.getY());            
            editorController.handleMousePressed(drawing_Pane, startingLocation, endLocation, toolSelected);

        });
        drawing_Pane.setOnMouseDragged(e -> {
            endLocation = new Point2D(e.getX(), e.getY());
            editorController.handleMouseDragged(startingLocation, endLocation, toolSelected);
        });

        drawing_Pane.setOnMouseReleased(e -> {                        
            editorController.handleMouseReleased(drawing_Pane, startingLocation, endLocation, toolSelected);            
        });
        
        // ADD CONTAINER TO WORKSPACE
        workspace = new Pane();
        workspace.getChildren().add(workspaceContainer);               
        // Style the GUI
        initStyle();
        
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {

        for(Button btn : shapeEditor_Btns) {
            btn.getStyleClass().add(SHAPE_EDIT_BTN_STYLE_CLASS);
            btn.setMinSize(50, 20);
        }
                
        tools_Pane.getStyleClass().add(TOOLS_PANE_STYLE_CLASS);
        shapeEditor_Pane.getStyleClass().add(HBOX_STYLE_CLASS);
        moveBtn_Pane.getStyleClass().add(HBOX_STYLE_CLASS);
        fillColor_Pane.getStyleClass().add(VBOX_STYLE_CLASS);
        outlineColor_Pane.getStyleClass().add(VBOX_STYLE_CLASS);
        outlineThickness_Pane.getStyleClass().add(VBOX_STYLE_CLASS);
        backgroundColor_Pane.getStyleClass().add(VBOX_STYLE_CLASS);
        snapshot_Pane.getStyleClass().add(VBOX_STYLE_CLASS);
        backgroundColor_lb.getStyleClass().add(LABEL_STYLE_CLASS);
        fillColor_lb.getStyleClass().add(LABEL_STYLE_CLASS);
        outlineColor_lb.getStyleClass().add(LABEL_STYLE_CLASS);
        outlineThickness_lb.getStyleClass().add(LABEL_STYLE_CLASS);        
    }
    
    public Color getBackgroundColor() {return backgroundColorPicker.getValue(); }    
    public ColorPicker getOutlineColorPicker() { return outlineColorPicker; }    
    public Slider getOutlineThickness_slider() {return outlineThickness_slider; }    
    public ColorPicker getFillColorPicker() { return fillColorPicker; }
    public Pane getDrawingPane() {return drawing_Pane;}
    
    public void setBackgroundColor(Color color) {backgroundColorPicker.setValue(color); }
    public void setFillColorPicker(Color color) {fillColorPicker.setValue(color); }
    public void setOutlineColorPicker(Color color) {  outlineColorPicker.setValue(color); }    
    public void setOutlineThickness_slider(double val) { outlineThickness_slider.setValue(val); }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {        
        // disable editing
        editorController.enable(false);
        
        toolSelected = IDLE;
        
        if(dataManager.getShapes()!=null) {
            ArrayList<Shape> shapes = dataManager.getShapes();
            for(int i = 0; i < shapes.size(); i++) {              
                if(shapes.get(i) instanceof Ellip) {
                    Ellip el = (Ellip)shapes.get(i);
                    el.setOnMousePressed(e->{
                        el.handleSelect(toolSelected, editorController, this);
                    });
                    el.setOnMouseDragged(e->{
                        el.handleDrag(toolSelected, e.getX(), e.getY());
                    });
                    drawing_Pane.getChildren().add(el);
                } else {
                    Rect r = (Rect)shapes.get(i);
                    r.setOnMousePressed(e->{
                        r.handleSelect(toolSelected, editorController, this);
                    });
                    r.setOnMouseDragged(e->{
                        r.handleDrag(toolSelected, e.getX(), e.getY());
                    });
                    drawing_Pane.getChildren().add(r);
                }                
            }
        }
        drawing_Pane.setStyle("-fx-background-color: white");
        backgroundColorPicker.setValue(dataManager.getBgColor());
        String color = "-fx-background-color:#" + dataManager.getBgColor().toString().substring(2,dataManager.getBgColor().toString().length()-2);        
        System.out.println("color: " + color);
        drawing_Pane.setStyle(color);
        
        if(drawing_Pane.getChildren()!=null) {
            for(int i = 0; i < drawing_Pane.getChildren().size(); i++) {
                // update controls to reflect the last edited shape
                Shape s = (Shape)drawing_Pane.getChildren().get(i);
                fillColorPicker.setValue((Color)s.getFill());
                outlineThickness_slider.setValue(s.getStrokeWidth());
                if(s instanceof Rect) {            
                    outlineColorPicker.setValue((Color)((Rect)s).getStrokeColor());
                } else {
                    outlineColorPicker.setValue((Color)((Ellip)s).getStrokeColor());
                }
            }            
        }
        // enable all btns
        for(Button b : disableable_Btns) {
            b.setDisable(false);
        }
        
        // disable btns on startup
        //selectionTool_Btn.setDisable(true);
        remove_Btn.setDisable(true);
        moveToBack_Btn.setDisable(true);
        moveToFront_Btn.setDisable(true);
        
        // reset cursor type
        drawing_Pane.setCursor(Cursor.DEFAULT);
        
        // enable editing
        editorController.enable(true);
    }
    
}